const path = require("path");
const fs = require("fs");
const WebpackHookPlugin = require("webpack-hook-plugin");
module.exports = {
   entry: {
      "webpack/first": "./source/webpack/first.js",
   },
   output: {
      filename: "[name].js",
      path: path.resolve(__dirname, "dist"),
   },
   mode: "production",
   devtool: "inline-source-map",
   plugins: [
      new WebpackHookPlugin({
         onBuildStart: [`node webpack.config.onBuildStart.js`],
         onBuildEnd: [`node webpack.config.onBuildEnd.js`],
      }),
   ],
};
