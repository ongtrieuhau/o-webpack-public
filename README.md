# o-webpack

[DOCUMENTATION](https://webpack.js.org/concepts/)

##### Init project

-  npm init

##### Install webpack CLI

-  `npm install webpack webpack-cli webpack-hook-plugin --save-dev`
-  `npm install -g webpack webpack-cli`
-  `npm install --save-dev webpack-hook-plugin`

##### Config webpack

-  create file `webpack.config.js`
-  create file `webpack.onBuildEnd.js`
-  create file `webpack.onBuildStart.js`
-  config command run in `package.json`: `npx webpack --config ./webpack.config.js`
-  create folder `source`: contains source code
-  create folder `dist`: contains build output

##### Run webpack

-  `npx webpack --config ./webpack.config.js`
-  `npm run buildwebpackStart`
